$(document).ready(function () {
    // show hidden menu
   $('.btn-menu').click(function () {
      $('.navbar-collapse').addClass('is_open');
   });
   $('.btn-close-menu').click(function () {
      $('.navbar-collapse').removeClass('is_open');
   });

   // show indicator
   var h_hvl = $('.hero-vlp').height();
   $(window).scroll(function () {
      if($(this).scrollTop() > h_hvl){
         $('.indicator').addClass('is_show');
      }else{
         $('.indicator').removeClass('is_show');
      }
   });

   //show popup text
   $("[data-popup-copy]").on('click',function () {
      var data_popup = $(this).data('popup-copy');
      $('.text-popup').addClass('is_open');
      $('.text-popup .content p').html(data_popup);
   });
   $('.text-popup .close').click(function () {
      $('.text-popup').removeClass('is_open');
   });

   //360view
   window.onload = init;
   var car;
   function init(){

       car = $('.car').ThreeSixty({
           totalFrames: 36, // Total no. of image you have for 360 slider
           endFrame: 36, // end frame for the auto spin animation
           currentFrame: 1, // This the start frame for auto spin
           imgList: '.threesixty_images', // selector for image list
           progress: '.spinner', // selector to show the loading progress
           imagePath:'/assets/images/car/', // path of the image assets
           filePrefix: '', // file prefix if any
           ext: '.jpg', // extention for the assets
           
           navigation: true
       });

   }
   $('.threesixty').click(function () {
      $('.threesixty_icon').fadeOut(300);
   });


   //function tabs
    tabs('.ds1 .tabs li','.ds1 .tab-image',0);
    tabs('.ds2 .tabs li','.ds2 .tab-image',0);
    tabs('.performance .tabs li','.performance .tab-image',0);
    tabs('.tech .tabs li','.tech .tab-image',0);

    function tabs(tab_select, tab_content, index_active) {
       $(tab_content).hide();
       $(tab_content).eq(index_active).show();
       $(tab_select).eq(index_active).addClass('active');
       $(tab_select).on('click',function () {
          $(tab_select).removeClass('active');
          $(this).addClass('active');
          var tab_index = $(this).index();
          $(tab_content).hide();
          $(tab_content).eq(tab_index).show();
       });
    }

    // verticaltabs line active
    line_active('.ds1 .tabs li','.ds1 .line-active');
    line_active('.ds2 .tabs li','.ds2 .line-active');
    line_active('.performance .tabs li','.performance .line-active');
    line_active('.tech .tabs li','.tech .line-active');
    function line_active(tab_current,line){
        $(tab_current).on('click',function () {
           $(tab_current).removeClass('active');
           $(this).addClass('active');
           var height_tab = $(this).height();
           var relY = $(this).offset().top - $(this).parent().offset().top;
           $(line).css({'top':relY, 'height':height_tab});
        });
    }

    $(window).on('load',function () {
        let h_line = $('.tabs li.active').height();
       $('.line-active').css('height',h_line);
    });

    //experience
    $(window).on('load',function () {
        var experience_item = $('.experience-item li').length;
        var i = 2;
       $('.service-wrapper ul li:first-child').addClass('active');
       var timer = setInterval(function () {
            if(i > experience_item){
                i = 1;
            }
            $('.service-wrapper ul li').removeClass('active');
            $('.service-wrapper ul li:nth-child('+i+')').addClass('active');
            i++;
        },4000);

        $('.experience-item li').on('click',function () {
            clearInterval(timer);
            var index_item = $(this).index() + 1;
            $('.service-wrapper ul li').removeClass('active');
            $('.service-wrapper ul li:nth-child('+index_item+')').addClass('active');
        });
    });

    //scroll
    $(window).scroll(function () {
       if($(this).scrollTop() > 800){
           $('.btn-top').fadeIn(300);
       }else{
           $('.btn-top').fadeOut(300);
       }
    });
    $('.btn-top').on('click',function () {
        $('html,body').animate({scrollTop: 0},1000);
    });

    //animation
    $(window).on('load',function () {
        $('.js-visual-wrapper').css({'visibility':'hidden','opacity':'0','transform':'matrix(1.5, 0, 0, 1.5, 0, 200)'});
        $('.js-animation-item').css({'visibility':'hidden','opacity':'0','transform':'matrix(1, 0, 0, 1, 0, 200)'});
        $('.js-data-type').css({'visibility':'hidden','opacity':'0','transform':'matrix(1, 0, 0, 1, 0, -60)'});
        $('.js-related-item').css({'visibility':'hidden','opacity':'0','transform':'matrix(1, 0, 0, 1, 0, 120)'});
        $('.js-animation-short').css({'visibility':'hidden','opacity':'0','transform':'matrix(1, 0, 0, 1, 0, 50)'});
    });

    $.fn.isInViewport = function() {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight() - 300;

        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height() - 300;

        return elementBottom > viewportTop && elementTop < viewportBottom;
    };

    $.fn.isInViewport1 = function() {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight() + 50;

        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height() + 50;

        return elementBottom > viewportTop && elementTop < viewportBottom;
    };

    $(window).on('resize scroll load', function() {
        $('.js-animation-item,.js-data-type,.js-related-item,.js-animation-short').each(function() {
            if ($(this).isInViewport1()) {
                $(this).css({'visibility':'inherit','opacity':'1','transform':'matrix(1, 0, 0, 1, 0, 0)'});
            }
        });
        $('.js-visual-wrapper').each(function () {
           if($(this).isInViewport()){
               $(this).css({'visibility':'inherit','opacity':'1','transform':'matrix(1, 0, 0, 1, 0, 0)'});
           }
        });

    });

    // indicator section
    $('.indicator li a').on('click',function () {
       let id_anchor = $(this).attr('href');
       $('html,body').animate({scrollTop: $(id_anchor).offset().top - 79},1000);
       $('.indicator li a').removeClass('active');
       $(this).addClass('active');
    });
    $(window).on('scroll',function () {
        $('.anchor').each(function () {
            if($(this).isInViewport1()){
                var id_anchor = $(this).attr('id');
                $('.indicator li a').removeClass('active');
                $(".indicator li a[href$='"+id_anchor+"']").addClass('active');
            }else{

            }
        });
    });
});